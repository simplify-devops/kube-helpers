# kube-helpers

# kube-helpers

## How to use it

```yaml
stages:
  - 🚢k8s-deploy

include:
  - project: 'simplify-devops/kube-helpers'
    ref: master
    file: 'k8s.dsl.yml'
# 👋 it's a "before script"

variables:
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80

🚀:deploy:service:
  stage: 🚢k8s-deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual
  allow_failure: true
  environment:
    name: prod/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
  script:
    - generate_manifest
    - apply

❌:remove:service:
  stage: 🚢k8s-deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual
  allow_failure: true
  environment:
    name: prod/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
  script:
    - delete_deployment

```
