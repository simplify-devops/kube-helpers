FROM alpine:3.10

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

# Note: Latest version of kubectl may be found at:
# https://github.com/kubernetes/kubernetes/releases
ENV KUBE_LATEST_VERSION="v1.17.0"
# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v3.0.2"

RUN apk add --no-cache ca-certificates bash git openssh curl jq httpie \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm \
    && apk add gettext libintl # install envsubst

RUN mkdir k8s-templates
RUN mkdir kube

COPY simple.web.app.template.yaml k8s-templates/simple.web.app.template.yaml
#COPY web.app.template.yaml k8s-templates/web.app.template.yaml
#COPY web.app.color.template.yaml k8s-templates/web.app.color.template.yaml
#COPY ingress.color.to.prod.template.yaml k8s-templates/ingress.color.to.prod.template.yaml

CMD ["/bin/sh"]

